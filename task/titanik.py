import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv('train.csv')
    return df


def get_filled():
    df = get_titatic_dataframe()

    # Extracting the titles from the 'Name' column
    df['Title'] = df['Name'].str.extract('([A-Za-z]+\.)', expand=False)

    # Calculating the median age for each title group
    title_groups = df.groupby('Title')['Age'].median().to_dict()

    # Filtering the results for the specified titles
    specified_titles = ["Mr.", "Mrs.", "Miss."]
    result = [(title, df[df['Title'] == title]['Age'].isnull().sum(), round(age)) for title, age in title_groups.items() if title in specified_titles]

    return result

filled_values = get_filled()
print(filled_values)
